Gnop ist ein einfacher Clone des bekannten Spiels Pong. 
Sinn war es, ein einfaches Spiel zu programmieren und dabei mehrere Techniken zu probieren. 
Daher ist nicht zu erwarten, dass der Quellcode besonders schön und gut konstruiert ist.

Gnop ist rein auf der Basis von SFML programmiert. Daher ist für einen erfolgreichen Build nicht mehr als SFML erforderlich.
Compiliert wurde Gnop erfolgreich mit 
- SFML 2.2 und VS 2013