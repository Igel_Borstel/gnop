#include "../include/parser.hpp"
#include <iostream>

using std::endl;

namespace gnop
{

	Parser::Parser(std::string file, Config& cfg)
		: filepath(file), config(cfg)
	{
	}

	void Parser::writeDefaultConfig()
	{
		Config defaultConfig(800, 400, "gfx/ui/windowicon.png", 16, 96, 8.f, 800, 400, 8, 200, 792, 200);
		write(defaultConfig);
	}

	void Parser::writeConfig()
	{
		write(config);
	}

	void Parser::write(Config& cfg)
	{
		std::ofstream file;
		file.open(filepath.c_str(), std::ios::out);
		if(!file)
		{
			std::cerr << "Configdatei konnte nicht geoeffnet werden" << endl;
		}
		file << "#Configdatei fuer GNOP\n# # oder ; dienen als Kommentare\n" << endl;
		file << "# Loeschen der Datei fuert zum zuruecksetzen auf die Defaultwerte" << endl;
		file << endl << endl;
		file << "windowwidth=" << cfg.getGameWidth() << endl;
		file << "windowhight=" << cfg.getGameHight() << endl;
		file << "iconpath=" << cfg.getIconpath() << endl;
		file << "paddlewidth=" << cfg.getPaddleWidth() << endl;
		file << "paddlehight=" << cfg.getPaddleHight() << endl;
		file << "ballradius=" << cfg.getBallRadius() << endl;
		file << "paddlespeed=" << cfg.getPaddleSpeed() << endl;
		file << "ballspeed=" << cfg.getBallSpeed() << endl;
		file << "p1x=" << cfg.getPaddle1XPos() << endl;
		file << "p1y=" << cfg.getPaddle1YPos() << endl;
		file << "p2x=" << cfg.getPaddle2XPos() << endl;
		file << "p2y=" << cfg.getPaddle2YPos() << endl;
	}

	void Parser::split(const std::string& src, std::string& var, std::string& val)
	{
		char zeichen;
		bool value = false;
		for(size_t i = 0; i < src.size(); ++i)
		{
			zeichen = src.at(i);
			if(zeichen == '=')
			{
				value = true;
			}
			else if(value)
			{
				val += zeichen;
			}
			else
			{
				var += zeichen;
			}
		}
	}

	void Parser::readConfig()
	{
		std::string line;
		std::ifstream file;
		file.open(filepath.c_str(), std::ios::in);
		if(!file)
		{
			std::cerr << "Configdatei konnte nicht geoeffnet werden" << std::endl;
		}
		while(getline(file, line))
		{
			if(line.size() > 1)
			{
				if(line.c_str()[0] == '#' || line.c_str()[0] == ';')
				{
					continue;
				}
				std::string variable;
				std::string value;
				split(line, variable, value);
				if(variable == "windowhight")
				{
					config.setGameHight(atoi(value.c_str()));
				}
				else if(variable == "windowwidth")
				{
					config.setGameWidth(atoi(value.c_str()));
				}
				else if(variable == "iconpath")
				{
					config.setIconpath(value);
				}
				else if(variable == "paddlewidth")
				{
					config.setPaddleWidth(atoi(value.c_str()));
				}
				else if(variable == "paddlehight")
				{
					config.setPaddleHight(atoi(value.c_str()));
				}
				else if(variable == "ballradius")
				{
					config.setBallRadius((float)atof(value.c_str()));
				}
				else if(variable == "paddlespeed")
				{
					config.setPaddleSpeed(atoi(value.c_str()));
				}
				else if(variable == "ballspeed")
				{
					config.setBallSpeed(atoi(value.c_str()));
				}
				else if(variable == "p1x")
				{
					config.setPaddle1XPos(atoi(value.c_str()));
				}
				else if(variable == "p1y")
				{
					config.setPaddle1YPos(atoi(value.c_str()));
				}
				else if(variable == "p2x")
				{
					config.setPaddle2XPos(atoi(value.c_str()));
				}
				else if(variable == "p2y")
				{
					config.setPaddle2YPos(atoi(value.c_str()));
				}
			}
			else
			{
				continue;
			}
		}
	}
}