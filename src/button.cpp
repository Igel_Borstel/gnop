#include "../include/button.hpp"
#include "../include/game.hpp"
#include <iostream>
#include <cstdlib>

#ifdef _WIN32
#include <Windows.h>
#endif //_WIN32

namespace gnop
{
	Button::Button(sf::Vector2i pos, std::string normal, std::string active, std::string hover, std::string text, std::string font, int w, int h, std::function<void()> cb, Game& g)
		: pos1(pos), game(g), width(w), hight(h), callback(cb)
	{
		sf::Image image;
		image.create(width, hight, sf::Color::Blue);
		if(!normalTexture.loadFromFile(normal))
		{
			normalTexture.loadFromImage(image);
		}
		if(!activeTexture.loadFromFile(active))
		{
			activeTexture.loadFromImage(image);
		}
		if(!hoverTexture.loadFromFile(hover))
		{
			hoverTexture.loadFromImage(image);
		}

		if(buttonFont.loadFromFile(font))
		{
			buttonText.setFont(buttonFont);
			buttonText.setColor(sf::Color::Black);
		}
		else
		{
		#ifdef _WIN32 //load alternativ font on windows
			const int BUFFERSIZE = 30;
			TCHAR envvar[BUFFERSIZE] = " ";
			GetEnvironmentVariable("windir", envvar, sizeof(envvar));
		
			if(envvar != nullptr && envvar[0] != ' ')
			{
				std::string envpath;
				envpath = envvar;
				if(!envpath.empty())
				{
					buttonFont.loadFromFile(envpath + std::string("/fonts/arial.ttf"));
					buttonText.setFont(buttonFont);
					buttonText.setColor(sf::Color::White);
				}
			}
		#endif //_WIN32
		}
		buttonText.setString(text);

		if(!(normalTexture.getSize() == activeTexture.getSize() && normalTexture.getSize() == hoverTexture.getSize()))
		{
		}

		pos2.x = pos1.x + width;
		pos2.y = pos1.y + hight;

		sprite.setPosition((float)pos1.x, (float)pos1.y);
		sprite.setTexture(normalTexture);
		sprite.setScale((float)width / normalTexture.getSize().x, (float)hight / normalTexture.getSize().y);

		buttonText.setPosition((float)pos1.x, (float)pos1.y);
		buttonText.setCharacterSize(100);
		while(buttonText.getLocalBounds().width > width || buttonText.getLocalBounds().height > hight)
		{
			buttonText.setCharacterSize(buttonText.getCharacterSize() - 1);
		}
	}

	Button::Button(sf::Vector2i pos, std::string normal, std::string text, std::string font, int w, int h, std::function<void()> cb, Game& g)
		: Button(pos, normal, normal, normal, text, font, w, h, cb, g)
	{
	}

	Button::Button(sf::Vector2i pos, std::string normal, std::string hover, std::string text, std::string font, int w, int h, std::function<void()> cb, Game& g)
		: Button(pos, normal, normal, hover, text, font, w, h, cb, g)
	{

	}

	Button::~Button()
	{
	}

	bool Button::isOnButton(sf::Vector2i pos)
	{
		return ((pos.x >= pos1.x && pos.x <= pos2.x) && (pos.y >= pos1.y && pos.y <= pos2.y)) ? true : false;
	}

	void Button::setActive()
	{
		sprite.setTexture(activeTexture);
	}

	void Button::setHover()
	{
		sprite.setTexture(hoverTexture);
	}

	void Button::setNormal()
	{
		sprite.setTexture(normalTexture);
	}

	void Button::draw()
	{
		game.getWindow().draw(sprite);
		game.getWindow().draw(buttonText);
	}

	void Button::update(sf::Time deltaTime)
	{
		if((oldTime += deltaTime) > clickTime)
		{
			if(isOnButton(sf::Mouse::getPosition(game.getWindow())))
			{
				if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					setActive();
					callback();
					oldTime = sf::seconds(0);
				}
				else
				{
					setHover();
				}
			}
			else
			{
				setNormal();
			}
		}
	}
}