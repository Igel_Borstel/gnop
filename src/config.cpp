#include "../include/config.hpp"

namespace gnop
{
	Config::Config(int gwidth, int ghight, std::string path, int pwidth, int phight,
		float bradius, int pSpeed, int bSpeed, int p1X, int p1Y, int p2X, int p2Y)
		: gameWidth(gwidth), gameHight(ghight), iconpath(path), paddleWidth(pwidth), paddleHight(phight),
		ballRadius(bradius), paddleSpeed(pSpeed), ballSpeed(bSpeed), paddle1XPos(p1X), paddle1YPos(p1Y), paddle2XPos(p2X), paddle2YPos(p2Y)
	{

	}

	Config::Config()
	{
	}
}