#ifndef CONFIG_HPP
#define CONFIG_HPP

#include <string>

namespace gnop
{
	class Config
	{
		int gameWidth;
		int gameHight;
		std::string iconpath;
		
		int paddleWidth;
		int paddleHight;

		float ballRadius;

		int paddleSpeed;
		int ballSpeed;

		int paddle1XPos;
		int paddle1YPos;
		int paddle2XPos;
		int paddle2YPos;
	public:
		Config();
		Config(int, int, std::string, int, int, float, int, int, int, int, int, int);
		int getGameWidth() const;
		int getGameHight() const;
		const std::string& getIconpath() const;
		int getPaddleWidth() const;
		int getPaddleHight() const;
		float getBallRadius() const;
		int getPaddleSpeed() const;
		int getBallSpeed() const;
		int getPaddle1XPos() const;
		int getPaddle1YPos() const;
		int getPaddle2XPos() const;
		int getPaddle2YPos() const;

		void setGameWidth(int);
		void setGameHight(int);
		void setIconpath(std::string);
		void setPaddleWidth(int);
		void setPaddleHight(int);
		void setBallRadius(float);
		void setPaddleSpeed(int);
		void setBallSpeed(int);
		void setPaddle1XPos(int);
		void setPaddle1YPos(int);
		void setPaddle2XPos(int);
		void setPaddle2YPos(int);
	};

	inline int Config::getGameWidth() const { return gameWidth; }
	inline int Config::getGameHight() const	{ return gameHight;	}
	inline const std::string& Config::getIconpath() const { return iconpath; }
	inline int Config::getPaddleWidth() const { return paddleWidth; }
	inline int Config::getPaddleHight() const { return paddleHight; }
	inline float Config::getBallRadius() const { return ballRadius; }
	inline int Config::getPaddleSpeed() const { return paddleSpeed; }
	inline int Config::getBallSpeed() const { return ballSpeed; }
	inline int Config::getPaddle1XPos() const { return paddle1XPos; }
	inline int Config::getPaddle1YPos() const { return paddle1YPos; }
	inline int Config::getPaddle2XPos() const { return paddle2XPos; }
	inline int Config::getPaddle2YPos() const { return paddle2YPos; }

	inline void Config::setGameWidth(int width) { gameWidth = width; }
	inline void Config::setGameHight(int hight) { gameHight = hight; }
	inline void Config::setIconpath(std::string path) { iconpath = path; }
	inline void Config::setPaddleWidth(int width) { paddleWidth = width; }
	inline void Config::setPaddleHight(int hight) { paddleHight = hight; }
	inline void Config::setBallRadius(float radius) { ballRadius = radius; }
	inline void Config::setPaddleSpeed(int speed) { paddleSpeed = speed; }
	inline void Config::setBallSpeed(int speed) { ballSpeed = speed; }
	inline void Config::setPaddle1XPos(int x) { paddle1XPos = x; }
	inline void Config::setPaddle1YPos(int y) { paddle1YPos = y; }
	inline void Config::setPaddle2XPos(int x) { paddle2XPos = x; }
	inline void Config::setPaddle2YPos(int y) { paddle2YPos = y; }
}

#endif //CONFIG_HPP