#ifndef GAME_HPP
#define GAME_HPP

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <deque>
#include "config.hpp"
#include "parser.hpp"
#include "button.hpp"
#include <vector>

/*
 *Microsoft setzt mehr auf den OpenSource Zug und wandelt sein Unternehmen
 *Das .net Framework von Microsoft steht kurz vor dem Untergang da es nun Opensource ist. Dies passierte bei einigen Projekten in der Vergangenheit.
 *Microsoft ist nicht in der Lage Copy and Paste in der Konsole einzubauen.
 *Der Visual C++ Compiler unst�tzt den C++11/14 Standard nicht komplett.
 */

namespace gnop
{
	class Game
	{
		//Enums
	public:
		enum class State
		{
			playing,
			pausing,
			mainMenu
		};

	private:
		Config config;
		Parser parser;
		sf::RenderWindow gameWindow;

		sf::RectangleShape paddle1;
		sf::RectangleShape paddle2;
		sf::CircleShape ball;

		sf::Clock gameClock;
		const sf::Time tick = sf::seconds(0.1f);
		State gameState;

		std::deque<sf::Keyboard::Key> pressedKeys;
		bool running;
		float angle;

		std::vector<Button*> mainMenuButtons;
		std::vector<Button*> pauseMenuButtons;
			
		void onPlaying();
		void updateGame();
		void renderGame();

		void onPausing();
		void updatePauseMenuButtons();
		void renderPauseMenu();

		void onMainMenu();
		void updateMainMenuButtons();
		void renderMainMenu();

	public:
		Game();
		~Game();
		int loop();
		sf::RenderWindow& getWindow();

		void startGame();
		void quitGame();
		void resumeGame();
		void gotoMainMenu();
	};

	inline sf::RenderWindow& Game::getWindow() { return gameWindow; }
}

#endif //GAME_HPP